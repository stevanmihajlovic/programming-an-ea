﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public abstract class AdultSelection
    {
        public abstract void selectAdults(Population children, Population parents);
    }
}
