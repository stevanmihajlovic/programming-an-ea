﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class FullGeneration : AdultSelection
    {
        public FullGeneration()
        {

        }
        public override void selectAdults(Population children, Population parents)
        {
            var sorted = children.individuals.OrderByDescending(item => item.fitness).ToList(); //sorting because of ranking
            parents.individuals.Clear();
            parents.populationSize = children.populationSize;
            foreach (Individual i in sorted)
                parents.individuals.Add(i);
            parents.updatePopulationData();
        }
    }
}
