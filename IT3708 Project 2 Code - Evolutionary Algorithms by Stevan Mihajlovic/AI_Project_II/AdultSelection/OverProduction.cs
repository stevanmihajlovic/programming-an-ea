﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class OverProduction : AdultSelection
    {
        
        public OverProduction()
        {

        }
        public override void selectAdults(Population children, Population parents)
        {
            var sorted = children.individuals.OrderByDescending(item => item.fitness).ToList();
            int i=0;

            parents.individuals.Clear();
            parents.populationSize = children.populationSize / Form1.adultselectionscale;
            foreach (Individual individual in sorted)
            {
                parents.individuals.Add(individual);
                i++;
                if ( i > parents.populationSize)
                    break;                
            }
            parents.updatePopulationData();
        }
    }
}
