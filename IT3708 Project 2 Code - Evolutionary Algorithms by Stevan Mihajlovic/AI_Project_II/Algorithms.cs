﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public class Algorithms
    {
        public static double crossOverRate=0.5;
        public static double mutationRate=0.02; //not per individual but per bit
        public static int tournamentsize=5;
        public static bool elitism=false;
        public static Random r = new Random();
        public Algorithms(bool e)   //smisli konstruktore i kako da postaivis parametre, setteri mozda koji ce moci da promene static
        {
            elitism = e;
        }
        public static Population evolvePopulation(Population population, int type)
        {
            Population newPopulation = new Population(population.populationSize);
            newPopulation.genomLength = population.genomLength;
            //vidi za inicijalizaciju genesa
            int i = 0;
            if (elitism)
            {
                newPopulation.individuals[0] = population.getFittest();
                i++;
            }
            while (i<population.populationSize)
            {
                if (type == 0)
                {
                    Individual a = tournamentSelection(population);
                    Individual b = tournamentSelection(population);
                    Individual newIndividual = crossOver(a, b);
                    mutation(newIndividual);
                    newPopulation.individuals[i] = newIndividual;   //na osnovu adult selection
                }
                if (type == 1)
                {

                }
                i++;
            }

            //nakon kreiranja nove populacije treba da odlucim da li da skroz izbacim sve prethodne roditelje ili da zadrzim neke i da mesam ili kako vec
            return newPopulation;

        }
        public static Individual crossOver(Individual a, Individual b) //uniform crossover
        {
            Individual newIndividual = new Individual();
            newIndividual.genomLength = a.genomLength;
            newIndividual.genes = new BitArray(newIndividual.genomLength);
            for (int i = 0; i < a.genomLength; i++)
                if (r.NextDouble() < crossOverRate)
                    newIndividual.genes[i] = a.genes[i];
                else
                    newIndividual.genes[i] = b.genes[i];
            return newIndividual;
        }
        public static void mutation(Individual a)  //invertuje neki na osnovu verovatnoce
        {

            for (int i = 0; i < a.genomLength; i++)
                if (r.NextDouble() < mutationRate)
                    a.genes[i] = !a.genes[i];
            //or should i do the same as in constructor a.genes[i]=(byte)Math.Round(r.NextDouble());
        }
        public static Individual tournamentSelection(Population population)    //ispitati ovo, gde se ovde pita fitness ista?
        {
            Population newPopulation = new Population(tournamentsize);
            for (int i = 0; i < tournamentsize; i++)
                newPopulation.individuals[i] = population.individuals[(int)r.NextDouble() * population.populationSize];
            Individual newIndividual = newPopulation.getFittest();
            return newIndividual;
        }
    }
}
