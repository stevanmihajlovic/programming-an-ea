﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public class Fitness
    {
        public Fitness()
        {

        }
        public static int CalculateOneMax(Individual a)
        {
            BitArray solution = new BitArray(a.genomLength);    //uciniti ga nekako globalnim
            solution.SetAll(true);
            int fitness=0;
            for (int i = 0; i < a.genes.Length; i++)
                if (solution[i] == a.genes[i])
                    fitness++;
            return fitness;
        }
    }
}
