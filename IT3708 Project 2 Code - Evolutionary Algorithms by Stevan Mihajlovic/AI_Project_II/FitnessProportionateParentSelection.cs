﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class FitnessProportionateParentSelection : ParentSelection
    {
        public FitnessProportionateParentSelection()
        {

        }
        public override Individual selectParent(Population population, Random randomize)
        {
            double randomNumber = randomize.NextDouble() * population.totalFitness; //every individual has piece of cake, bigger fitess => bigger chance
            int index;
            for (index = 0; index < population.parents.Count && randomNumber > 0; ++index)
            {
                randomNumber -= population.parents.ElementAt(index).fitness;
            }
            return population.parents.ElementAt(index - 1);
        }
    }
}
