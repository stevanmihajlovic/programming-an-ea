﻿namespace AI_Project_II
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.textBoxPopulationSize = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxGenomLength = new System.Windows.Forms.TextBox();
            this.textBoxCrossOverRate = new System.Windows.Forms.TextBox();
            this.textBoxMutationRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxElitism = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxProblem = new System.Windows.Forms.ComboBox();
            this.comboBoxAdult = new System.Windows.Forms.ComboBox();
            this.comboBoxMate = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxAdultSelectionScale = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxTournamentSize = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.checkBoxRandomOneMax = new System.Windows.Forms.CheckBox();
            this.textBoxLolzcap = new System.Windows.Forms.TextBox();
            this.chartFitness = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxSSupriseSequence = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxLSupriseSequence = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxLastGeneration = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chartFitness)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxPopulationSize
            // 
            this.textBoxPopulationSize.Location = new System.Drawing.Point(132, 12);
            this.textBoxPopulationSize.Name = "textBoxPopulationSize";
            this.textBoxPopulationSize.Size = new System.Drawing.Size(100, 20);
            this.textBoxPopulationSize.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Set Population Size:";
            // 
            // textBoxGenomLength
            // 
            this.textBoxGenomLength.Location = new System.Drawing.Point(132, 38);
            this.textBoxGenomLength.Name = "textBoxGenomLength";
            this.textBoxGenomLength.Size = new System.Drawing.Size(100, 20);
            this.textBoxGenomLength.TabIndex = 2;
            // 
            // textBoxCrossOverRate
            // 
            this.textBoxCrossOverRate.Location = new System.Drawing.Point(132, 64);
            this.textBoxCrossOverRate.Name = "textBoxCrossOverRate";
            this.textBoxCrossOverRate.Size = new System.Drawing.Size(100, 20);
            this.textBoxCrossOverRate.TabIndex = 3;
            // 
            // textBoxMutationRate
            // 
            this.textBoxMutationRate.Location = new System.Drawing.Point(132, 90);
            this.textBoxMutationRate.Name = "textBoxMutationRate";
            this.textBoxMutationRate.Size = new System.Drawing.Size(100, 20);
            this.textBoxMutationRate.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Set Genom Length:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Set Cross-Over Rate:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Set Mutation Rate:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Select Problem:";
            // 
            // checkBoxElitism
            // 
            this.checkBoxElitism.AutoSize = true;
            this.checkBoxElitism.Location = new System.Drawing.Point(132, 439);
            this.checkBoxElitism.Name = "checkBoxElitism";
            this.checkBoxElitism.Size = new System.Drawing.Size(55, 17);
            this.checkBoxElitism.TabIndex = 10;
            this.checkBoxElitism.Text = "Elitism";
            this.checkBoxElitism.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(178, 487);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Select Adult Selection:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Select Mate Selection:";
            // 
            // comboBoxProblem
            // 
            this.comboBoxProblem.FormattingEnabled = true;
            this.comboBoxProblem.Items.AddRange(new object[] {
            "One Max",
            "LOLZ",
            "Global Surprising Sequence",
            "Local Surprising Sequence"});
            this.comboBoxProblem.Location = new System.Drawing.Point(132, 116);
            this.comboBoxProblem.Name = "comboBoxProblem";
            this.comboBoxProblem.Size = new System.Drawing.Size(142, 21);
            this.comboBoxProblem.TabIndex = 14;
            // 
            // comboBoxAdult
            // 
            this.comboBoxAdult.FormattingEnabled = true;
            this.comboBoxAdult.Items.AddRange(new object[] {
            "Full Generation Replacement",
            "Over Production",
            "Generation Mixing"});
            this.comboBoxAdult.Location = new System.Drawing.Point(132, 144);
            this.comboBoxAdult.Name = "comboBoxAdult";
            this.comboBoxAdult.Size = new System.Drawing.Size(142, 21);
            this.comboBoxAdult.TabIndex = 15;
            // 
            // comboBoxMate
            // 
            this.comboBoxMate.FormattingEnabled = true;
            this.comboBoxMate.Items.AddRange(new object[] {
            "Tournament Selection",
            "Fitness Proportionate",
            "Sigma Scaling",
            "Rank Selection"});
            this.comboBoxMate.Location = new System.Drawing.Point(132, 171);
            this.comboBoxMate.Name = "comboBoxMate";
            this.comboBoxMate.Size = new System.Drawing.Size(142, 21);
            this.comboBoxMate.TabIndex = 16;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(280, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(998, 329);
            this.listBox1.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 229);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Adult Selection Scale:";
            // 
            // textBoxAdultSelectionScale
            // 
            this.textBoxAdultSelectionScale.Location = new System.Drawing.Point(132, 226);
            this.textBoxAdultSelectionScale.Name = "textBoxAdultSelectionScale";
            this.textBoxAdultSelectionScale.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdultSelectionScale.TabIndex = 19;
            this.textBoxAdultSelectionScale.Text = "2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 255);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Tournament Size:";
            // 
            // textBoxTournamentSize
            // 
            this.textBoxTournamentSize.Location = new System.Drawing.Point(132, 252);
            this.textBoxTournamentSize.Name = "textBoxTournamentSize";
            this.textBoxTournamentSize.Size = new System.Drawing.Size(100, 20);
            this.textBoxTournamentSize.TabIndex = 21;
            this.textBoxTournamentSize.Text = "5";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 322);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Z for Lolz:";
            // 
            // checkBoxRandomOneMax
            // 
            this.checkBoxRandomOneMax.AutoSize = true;
            this.checkBoxRandomOneMax.Location = new System.Drawing.Point(132, 296);
            this.checkBoxRandomOneMax.Name = "checkBoxRandomOneMax";
            this.checkBoxRandomOneMax.Size = new System.Drawing.Size(130, 17);
            this.checkBoxRandomOneMax.TabIndex = 23;
            this.checkBoxRandomOneMax.Text = "Random for one max?";
            this.checkBoxRandomOneMax.UseVisualStyleBackColor = true;
            // 
            // textBoxLolzcap
            // 
            this.textBoxLolzcap.Location = new System.Drawing.Point(132, 319);
            this.textBoxLolzcap.Name = "textBoxLolzcap";
            this.textBoxLolzcap.Size = new System.Drawing.Size(100, 20);
            this.textBoxLolzcap.TabIndex = 24;
            this.textBoxLolzcap.Text = "21";
            // 
            // chartFitness
            // 
            chartArea1.Name = "ChartArea1";
            this.chartFitness.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartFitness.Legends.Add(legend1);
            this.chartFitness.Location = new System.Drawing.Point(280, 347);
            this.chartFitness.Name = "chartFitness";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "BestFit";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "AvgFit";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "SD";
            this.chartFitness.Series.Add(series1);
            this.chartFitness.Series.Add(series2);
            this.chartFitness.Series.Add(series3);
            this.chartFitness.Size = new System.Drawing.Size(403, 300);
            this.chartFitness.TabIndex = 25;
            this.chartFitness.Text = "chart1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 348);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "S for Suprise Sequence:";
            // 
            // textBoxSSupriseSequence
            // 
            this.textBoxSSupriseSequence.Location = new System.Drawing.Point(132, 345);
            this.textBoxSSupriseSequence.Name = "textBoxSSupriseSequence";
            this.textBoxSSupriseSequence.Size = new System.Drawing.Size(100, 20);
            this.textBoxSSupriseSequence.TabIndex = 27;
            this.textBoxSSupriseSequence.Text = "5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 374);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "L for Suprise Sequence:";
            this.label12.Visible = false;
            // 
            // textBoxLSupriseSequence
            // 
            this.textBoxLSupriseSequence.Location = new System.Drawing.Point(132, 371);
            this.textBoxLSupriseSequence.Name = "textBoxLSupriseSequence";
            this.textBoxLSupriseSequence.Size = new System.Drawing.Size(100, 20);
            this.textBoxLSupriseSequence.TabIndex = 29;
            this.textBoxLSupriseSequence.Text = "10";
            this.textBoxLSupriseSequence.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 645);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "Last generation:";
            // 
            // textBoxLastGeneration
            // 
            this.textBoxLastGeneration.Location = new System.Drawing.Point(15, 661);
            this.textBoxLastGeneration.Name = "textBoxLastGeneration";
            this.textBoxLastGeneration.Size = new System.Drawing.Size(1263, 20);
            this.textBoxLastGeneration.TabIndex = 31;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 707);
            this.Controls.Add(this.textBoxLastGeneration);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBoxLSupriseSequence);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxSSupriseSequence);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.chartFitness);
            this.Controls.Add(this.textBoxLolzcap);
            this.Controls.Add(this.checkBoxRandomOneMax);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxTournamentSize);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxAdultSelectionScale);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.comboBoxMate);
            this.Controls.Add(this.comboBoxAdult);
            this.Controls.Add(this.comboBoxProblem);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBoxElitism);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxMutationRate);
            this.Controls.Add(this.textBoxCrossOverRate);
            this.Controls.Add(this.textBoxGenomLength);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPopulationSize);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chartFitness)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxPopulationSize;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxGenomLength;
        private System.Windows.Forms.TextBox textBoxCrossOverRate;
        private System.Windows.Forms.TextBox textBoxMutationRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxElitism;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxProblem;
        private System.Windows.Forms.ComboBox comboBoxAdult;
        private System.Windows.Forms.ComboBox comboBoxMate;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxAdultSelectionScale;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxTournamentSize;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBoxRandomOneMax;
        private System.Windows.Forms.TextBox textBoxLolzcap;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartFitness;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxSSupriseSequence;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxLSupriseSequence;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxLastGeneration;
    }
}

