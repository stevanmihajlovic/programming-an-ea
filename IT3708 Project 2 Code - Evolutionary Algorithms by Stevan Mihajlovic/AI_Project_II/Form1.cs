﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace AI_Project_II
{
    public partial class Form1 : Form
    {
        Random randomize;
        public double crossOverRate;
        public double mutationRate; //not per individual but per bit
        public static int tournamentSize;
        public bool elitism;
        public static BitArray onemaxsolution;    //negde moras da ga postavis, ili sve na 1 ili na random
        public static double lolzcap;                  //i ovo isto
        public static int adultselectionscale;
        public static int ssuprisesequence;     //range or 0 to this value
        public static int lsuprisesequence;
        
        public List<double> avgfit;
        public List<double> bestfit;
        public List<double> sd;

        public Form1()
        {
            InitializeComponent();
            randomize = new Random();
            comboBoxProblem.SelectedIndex = 0;
            comboBoxMate.SelectedIndex = 0;
            comboBoxAdult.SelectedIndex = 0;
            avgfit = new List<double>();
            bestfit = new List<double>();
            sd = new List<double>();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            avgfit.Clear();
            bestfit.Clear();
            chartFitness.Series["SD"].Points.Clear();
            chartFitness.Series["BestFit"].Points.Clear();
            chartFitness.Series["AvgFit"].Points.Clear();
            //RUN WORLD
            try
            {
                int populationSize = Convert.ToInt32(textBoxPopulationSize.Text);
                int genomLength = Convert.ToInt32(textBoxGenomLength.Text);
                crossOverRate = Convert.ToDouble(textBoxCrossOverRate.Text);
                mutationRate = Convert.ToDouble(textBoxMutationRate.Text);
                
                tournamentSize = Convert.ToInt32(textBoxTournamentSize.Text); //videti kako ovo da podesis
                adultselectionscale = Convert.ToInt32(textBoxAdultSelectionScale.Text);
                lolzcap = Convert.ToInt32(textBoxLolzcap.Text);
                ssuprisesequence = Convert.ToInt32(textBoxSSupriseSequence.Text);
                lsuprisesequence = Convert.ToInt32(textBoxLSupriseSequence.Text);

                if (checkBoxRandomOneMax.Checked)
                {
                    onemaxsolution = new BitArray(genomLength);
                    for (int i = 0; i < genomLength; i++)
                        if (randomize.NextDouble() <= 0.5)
                            onemaxsolution[i] = false;
                        else
                            onemaxsolution[i] = true;
                }
                else
                    onemaxsolution = new BitArray(genomLength, true);

                elitism = checkBoxElitism.Checked;


                Population population = new Population(2*populationSize,genomLength,randomize,comboBoxProblem.SelectedIndex,comboBoxAdult.SelectedIndex,comboBoxMate.SelectedIndex);
                population.updateFitnessForAll();      //u zavisnosti od problema poziva se odredjeni update

                int counter=0;
                Population newpopulation = new Population();    //izbaci counter kasnije
                while (population.getMaxFit().fitness<population.genomLength && counter < 1000)    //vidi kako ovo kada vidis fitness kako se racuna
                {
                    listBox1.Items.Add("Generation: " + counter.ToString() + ", Best Individual: " + population.getMaxFit().Print() +
    ", his fitness" + population.getMaxFit().fitness + ", Average Fitness: " + population.averageFitness + ", Standard Deviation: " + population.standardDeviation);

                    bestfit.Add(population.getMaxFit().fitness);
                    avgfit.Add(population.averageFitness);
                    sd.Add(population.standardDeviation);
                    //nesto
                    population.adultSelection.selectAdults(population,newpopulation);

                    population.individuals.Clear();
                    int j=0;
                    if (elitism)
                    {
                        population.individuals.Add(newpopulation.getMaxFit());
                        population.individuals.Add(newpopulation.getMaxFit());
                        j = j + 2;
                    }

                    while (j < population.populationSize)
                    {
                        Individual[] newones = new Individual[2];
                        newones[0] = population.parentSelection.selectParent(newpopulation, randomize); //vidi sta za ovo
                        newones[1] = population.parentSelection.selectParent(newpopulation, randomize);
                        if (randomize.NextDouble() < crossOverRate)
                            newones = newones[0].crossover(randomize, newones[1]);  //kako ovde dobiju fitness 0
                        newones[0].mutation(randomize, mutationRate);
                        newones[1].mutation(randomize, mutationRate);
                        population.individuals.Add(newones[0]);
                        population.individuals.Add(newones[1]);
                        j = j + 2;
                    }
                    population.updateFitnessForAll();
                    counter++;
                }
                textBoxLastGeneration.Text=("Generation: " + counter.ToString() + ", Best Individual: " + population.getMaxFit().Print() +
", his fitness" + population.getMaxFit().fitness + ", Average Fitness: " + population.averageFitness + ", Standard Deviation: " + population.standardDeviation);
                bestfit.Add(population.getMaxFit().fitness);
                avgfit.Add(population.averageFitness);
                sd.Add(population.standardDeviation);
                for (int i = 0; i <= counter; i++)
                {
                    chartFitness.Series["SD"].Points.AddXY(i, Math.Round(sd[i]));
                    chartFitness.Series["BestFit"].Points.AddXY(i, bestfit[i]);
                    chartFitness.Series["AvgFit"].Points.AddXY(i, avgfit[i]);
                }
                chartFitness.Series["BestFit"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                chartFitness.Series["AvgFit"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                chartFitness.Series["SD"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

                chartFitness.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                chartFitness.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                
            }
            catch (Exception ec)
            {
                MessageBox.Show("Use numbers only");
            }
          
        }        
    }
}
