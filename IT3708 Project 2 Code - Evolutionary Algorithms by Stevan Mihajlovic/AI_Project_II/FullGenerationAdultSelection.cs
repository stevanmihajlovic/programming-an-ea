﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class FullGenerationAdultSelection : AdultSelection
    {
        public FullGenerationAdultSelection()
        {

        }
        public override void selectAdults(Population population)
        {
            foreach (Individual i in population.children)
                population.parents.Add(i);
        }
    }
}
