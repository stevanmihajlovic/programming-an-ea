﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    /*abstract*/ public abstract class BitArrayGtype : Individual
    {
        public BitArray genes;

        public BitArrayGtype()
        {
            
        }
        public BitArrayGtype(int size)
        {
            genomLength = size;
            genes = new BitArray(genomLength);
        }
        public BitArrayGtype(int n, Random r)
        {
            genomLength = n;
            genes = new BitArray(genomLength);
            for (int i = 0; i < genomLength; i++)
                if (r.NextDouble() <= 0.5)
                    genes[i] = false;
                else
                    genes[i] = true;
        }
        public override void mutation(Random r, double mutationRate)
        {
            for (int i = 0; i < genomLength; i++)
                if (r.NextDouble() < mutationRate)
                    genes[i] = !genes[i];
        }

        public override Individual[] crossover(Random r, Individual a)  //selektujes 2 pa ides prva.crossover(druga)
        {
            Individual[] newIndiv = new BitArrayGtype[2];
            newIndiv[0] = this.New();
            newIndiv[1] = this.New();

            int crossPoint = r.Next(genomLength);
            int i;
            for (i = 0; i < crossPoint; i++)    //ispitati!
            {
                ((BitArrayGtype) newIndiv[0]).genes[i] = ((BitArrayGtype) a).genes[i];
                ((BitArrayGtype) newIndiv[1]).genes[i] = genes[i];
            }
            for (; i < genomLength; i++)
            {
                ((BitArrayGtype) newIndiv[0]).genes[i] = genes[i];
                ((BitArrayGtype) newIndiv[1]).genes[i] = ((BitArrayGtype) a).genes[i];
            }

            return newIndiv;
        }
        public override string Print()
        {
            string a = "";
            for (int i = 0; i < genomLength; i++)
                if (genes[i])
                    a += 1;
                else
                    a += 0;
            return a;
        }
    }
}
