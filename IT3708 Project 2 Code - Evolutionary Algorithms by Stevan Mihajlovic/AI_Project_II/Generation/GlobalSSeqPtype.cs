﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class GlobalSSeqPtype : IntArrayGtype
    {
        public GlobalSSeqPtype()
        {

        }
        public GlobalSSeqPtype(int size) : base (size)
        {

        }
        public GlobalSSeqPtype(int n, Random r, int min, int max) : base(n, r, min, max)
        {

        }
        public GlobalSSeqPtype(int n, Random r) : base (n, r)
        {

        }
        public override Individual New()
        {
            return new GlobalSSeqPtype(genomLength);
        }
        public override double calculateFitness()
        {
            List<string> foundsequences = new List<string>();
            fitness = genomLength;
            for (int i = 0; i < genomLength-1; i++)
                for (int j = i + 1; j < genomLength;j++ )
                {
                    string temp = genes[i].ToString() + genes[j].ToString() + (j - i).ToString();
                    if (foundsequences.Contains(temp))
                        fitness--;
                    else
                        foundsequences.Add(temp);
                }
            return fitness;
        }
    }
}
