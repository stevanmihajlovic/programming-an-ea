﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public abstract class Individual         //phenotype
    {
        public int genomLength;
        public double fitness;
        abstract public void  mutation(Random r, double mutationRate);
        abstract public Individual[] crossover(Random r, Individual a);
        abstract public double  calculateFitness();
        public abstract Individual New();
        public abstract string Print();
    }
}
