﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public abstract class IntArrayGtype : Individual
    {
        public List<int> genes;
        public int minvalue;
        public int maxvalue;

        public IntArrayGtype()
        {
            
        }
        public IntArrayGtype(int size)
        {
            genomLength = size;
            genes = new List<int>(genomLength);
        }
        public IntArrayGtype(int n, Random r)
        {
            genomLength = n;
            genes = new List<int>(genomLength);
            for (int i = 0; i < genomLength; i++)
                genes.Add(r.Next());
        }
        public IntArrayGtype(int n, Random r, int min, int max)
        {
            genomLength = n;
            genes = new List<int>(genomLength);
            for (int i = 0; i < genomLength; i++)
                genes.Add(r.Next(min,max));
            maxvalue = max;
            minvalue = min;
        }
        public override void mutation(Random r, double mutationRate)
        {
            for (int i = 0; i < genomLength; i++)
                if (r.NextDouble() < mutationRate)
                    if (maxvalue != 0)
                        genes[i] = r.Next(minvalue, maxvalue);
                    else
                        genes[i] = r.Next();
        }

        public override Individual[] crossover(Random r, Individual a)  //selektujes 2 pa ides prva.crossover(druga)
        {
            Individual[] newIndiv = new IntArrayGtype[2];
            newIndiv[0] = this.New();
            newIndiv[1] = this.New();

            int crossPoint = r.Next(genomLength);
            int i;
            for (i = 0; i < crossPoint; i++)
            {
                ((IntArrayGtype)newIndiv[0]).genes.Add(((IntArrayGtype)a).genes[i]);
                ((IntArrayGtype)newIndiv[1]).genes.Add(genes[i]);

            }
            for (; i < genomLength; i++)
            {
                ((IntArrayGtype)newIndiv[0]).genes.Add(genes[i]);
                ((IntArrayGtype)newIndiv[1]).genes.Add(((IntArrayGtype)a).genes[i]);
              
            }
            if (maxvalue!=0)
            {
                ((IntArrayGtype)newIndiv[0]).maxvalue = maxvalue;
                ((IntArrayGtype)newIndiv[1]).maxvalue = maxvalue;
                ((IntArrayGtype)newIndiv[0]).minvalue = minvalue;
                ((IntArrayGtype)newIndiv[1]).minvalue = minvalue;
            }
            return newIndiv;
        }
        public override string Print()
        {
            string a = "";
            for (int i = 0; i < genomLength; i++)
                a = a + genes[i]+", ";
            return a;
        }
    }
}
