﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class LocalSSeqPtype : IntArrayGtype
    {
        public LocalSSeqPtype()
        {

        }
        public LocalSSeqPtype(int size) : base (size)
        {

        }
        public LocalSSeqPtype(int n, Random r, int min, int max) : base(n, r, min, max)
        {

        }
        public LocalSSeqPtype(int n, Random r) : base(n, r)
        {

        }
        public override Individual New()
        {
            return new LocalSSeqPtype(genomLength);
        }
        public override double calculateFitness()
        {
            List<string> foundsequences = new List<string>();
            fitness = genomLength;
            for (int i = 0; i < genomLength-1; i++)
                {
                    string temp = genes[i].ToString() + genes[i+1].ToString();
                    if (foundsequences.Contains(temp))
                        fitness--;
                    else
                        foundsequences.Add(temp);
                }
            return fitness;
        }
    }
}
