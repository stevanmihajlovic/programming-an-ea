﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public class LolzPtype : BitArrayGtype
    {
        public LolzPtype()
        {

        }
        public LolzPtype(int size) : base(size)
        {
            fitness = 0;
        }
        public LolzPtype(int size, Random r) : base(size, r)
        {
            fitness = 0;
        }
        public override double calculateFitness()
        {
            fitness = 1;
            bool first = genes[0];
            while ((fitness < genomLength) && (genes[(int)fitness] == first))
                fitness++;
            if (!first && fitness>Form1.lolzcap)
                fitness = Form1.lolzcap;
            //fitness = fitness / genomLength;
            return fitness; //da li ceo broj ili od 0 do 1, da li da vraca vrednost ili samo da updatuje
        }
        public override Individual New()
        {
            return new LolzPtype(genomLength);
        }
    }
}
