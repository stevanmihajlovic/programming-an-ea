﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public class OneMaxPtype : BitArrayGtype
    {
        public OneMaxPtype()
        {

        }
        public OneMaxPtype(int size) : base(size)
        {
            fitness = 0;
        }
        public OneMaxPtype(int size, Random r) : base(size,r)
        {
            fitness = 0;
        }
        public override double calculateFitness()
        {
            fitness = 0;
            for (int i = 0; i < genomLength; i++)
                if (Form1.onemaxsolution[i] == genes[i])
                    fitness++;
            //fitness = fitness / genomLength;
            return fitness ; //da li ceo broj ili od 0 do 1, da li da vraca vrednost ili samo da updatuje
        }
        public override Individual New()
        {
            return new OneMaxPtype(genomLength);
        }
    }
}
