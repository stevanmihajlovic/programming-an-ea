﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class Population
    {
        public List<Individual> individuals;
        public int populationSize;
        public int genomLength;
        public double totalFitness;
        public double averageFitness;
        public double standardDeviation;
        public AdultSelection adultSelection;   //napravi konstruktor
        public Random randomize;
        public ParentSelection parentSelection;

        public Population()         //kako moje nove generacije dobiju random???
        {
            individuals = new List<Individual>();
            totalFitness = 0;
        }
        
        public Population(Population p)
        {
            populationSize = p.populationSize;
            genomLength = p.genomLength;
            randomize = p.randomize;            //vidi dal treba
            totalFitness = 0;
            individuals = new List<Individual>();
        }
        public Population(int size,int n, Random r, int problemtype, int adulttype,int parenttype)
        {
            randomize = r;
            populationSize = size;
            genomLength = n;
            individuals = new List<Individual>();
            if (problemtype == 0)
                for (int i=0;i<populationSize;i++)
                    individuals.Add(new OneMaxPtype(genomLength,randomize));
            if (problemtype == 1)
                for (int i = 0; i < populationSize; i++)
                    individuals.Add(new LolzPtype(genomLength, randomize));
            if (problemtype == 2)
                 for (int i = 0; i < populationSize; i++)
                    individuals.Add(new GlobalSSeqPtype(genomLength, randomize,0,Form1.ssuprisesequence));
            if (problemtype == 3)
                for (int i = 0; i < populationSize; i++)
                    individuals.Add(new LocalSSeqPtype(genomLength, randomize, 0, Form1.ssuprisesequence));
            if (parenttype == 0)
                parentSelection = new TournamentSelection();
            if (parenttype == 1)
                parentSelection = new FitnessProportionate();
            if (parenttype == 2)
                parentSelection = new SigmaScaling();
            if (parenttype == 3)
                parentSelection = new RankSelection();
            if (adulttype == 0)
                adultSelection = new FullGeneration();
            if (adulttype == 1)
                adultSelection = new OverProduction();
            if (adulttype == 2)
                adultSelection = new GenerationMixing();
            totalFitness = 0;
        }
        public Individual getMaxFit()
        {
            Individual maxfit = individuals[0];
            for (int i = 1; i < populationSize; i++)
                if (maxfit.fitness < individuals[i].fitness)
                    maxfit = individuals[i];
            return maxfit;
        }
        public Individual getMinFit()
        {
            Individual minfit = individuals[0];
            for (int i = 1; i < populationSize; i++)
                if (minfit.fitness > individuals[i].fitness)
                    minfit = individuals[i];
            return minfit;
        }
        public void updateFitnessForAll()
        {
            foreach (Individual i in individuals)
                i.calculateFitness();
            updatePopulationData();
        }
        public void updatePopulationData()
        {
            totalFitness = 0;
            foreach (Individual i in individuals)
                totalFitness += i.fitness;
            averageFitness = totalFitness / populationSize;
            standardDeviation = 0;
            foreach (Individual i in individuals)
                standardDeviation += Math.Pow(i.fitness - averageFitness, 2);
            standardDeviation /= populationSize;
            standardDeviation = Math.Sqrt(standardDeviation);
        }
        
    }
}
