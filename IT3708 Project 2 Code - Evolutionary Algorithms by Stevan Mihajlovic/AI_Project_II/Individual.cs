﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public abstract class Individual         //phenotype
    {
        public int genomLength;
        public double fitness;
        abstract public void  mutation(Random randomize, double mutationRate);
        abstract public Individual[] crossover(Random randomize, Individual a);
        abstract public double  calculateFitness();
        public abstract Individual New();
        public abstract string Print();
        //public BitArray genes;      //genotype

        /*public Individual()
        {

        }
        public Individual(int n, Random r)
        {
            genomLength = n;
            genes = new BitArray(genomLength);
            for (int i = 0; i < genomLength; i++)
                if (r.NextDouble() <= 0.5)
                    genes[i] = false;
                else
                    genes[i] = true;
        }
        public int[] genotypeToPhenotype()  //ostavljamo za kasnije, shift, ovde implementiraj
        {
            return null;
        }
        //ubaciti fitness nekako
        
        public void updateFitness(int type)
        {
            if (type == 0) fitness = calculateOneMax();
        }
        public double calculateOneMax() //parametar solution neka bude
        {
            BitArray solution = new BitArray(genomLength);    //uciniti ga nekako globalnim
            solution.SetAll(true);
            double fitness = 0;
            for (int i = 0; i < genomLength; i++)
                if (solution[i] == genes[i])
                    fitness++;
            return fitness/genomLength; //da li ceo broj ili od 0 do 1
        }*/
    }
}
