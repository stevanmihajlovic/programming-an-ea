﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public abstract class IntArrayIndividual : Individual
    {
        public List<int> genes;
        public int minvalue;
        public int maxvalue;

        public IntArrayIndividual()
        {
            
        }
        public IntArrayIndividual(int size)
        {
            genomLength = size;
            genes = new List<int>(genomLength);
        }
        public IntArrayIndividual(int n, Random r)
        {
            genomLength = n;
            genes = new List<int>(genomLength);
            for (int i = 0; i < genomLength; i++)
                genes[i] = r.Next();
        }
        public IntArrayIndividual(int n, Random r, int min, int max)
        {
            genomLength = n;
            genes = new List<int>(genomLength);
            for (int i = 0; i < genomLength; i++)
                genes[i] = r.Next(min,max);
        }
        public override void mutation(Random randomize, double mutationRate)
        {
            for (int i = 0; i < genomLength; i++)
                if (randomize.NextDouble() < mutationRate)
                    if (maxvalue != 0)
                        genes[i] = randomize.Next(minvalue, maxvalue);
                    else
                        genes[i] = randomize.Next();
        }

        public override Individual[] crossover(Random randomize, Individual a)  //selektujes 2 pa ides prva.crossover(druga)
        {
            Individual[] newIndiv = new IntArrayIndividual[2];
            newIndiv[0] = this.New();
            newIndiv[1] = this.New();

            int crossPoint = randomize.Next(genomLength);
            int i;
            for (i = 0; i < crossPoint; ++i)
            {
                ((IntArrayIndividual)newIndiv[0]).genes[i] = ((IntArrayIndividual)a).genes[i];
                ((IntArrayIndividual)newIndiv[1]).genes[i] = genes[i];
            }
            for (; i < genomLength; ++i)
            {
                ((IntArrayIndividual)newIndiv[0]).genes[i] = genes[i];
                ((IntArrayIndividual)newIndiv[1]).genes[i] = ((IntArrayIndividual)a).genes[i];
            }
            if (maxvalue!=0)
            {
                ((IntArrayIndividual)newIndiv[0]).maxvalue = maxvalue;
                ((IntArrayIndividual)newIndiv[1]).maxvalue = maxvalue;
                ((IntArrayIndividual)newIndiv[0]).minvalue = minvalue;
                ((IntArrayIndividual)newIndiv[1]).minvalue = minvalue;
            }
            return newIndiv;
        }
        public override string Print()
        {
            string a = "";
            for (int i = 0; i < genomLength; i++)
                a += genes[i];
            return a;
        }
    }
}
