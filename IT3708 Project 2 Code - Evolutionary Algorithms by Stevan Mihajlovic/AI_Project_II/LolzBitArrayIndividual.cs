﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public class LolzBitArrayIndividual : BitArrayIndividual
    {
        public LolzBitArrayIndividual()
        {

        }
        public LolzBitArrayIndividual(int size) : base(size)
        {
            fitness = 0;
        }
        public LolzBitArrayIndividual(int size, Random r) : base(size, r)
        {
            fitness = 0;
        }
        public override double calculateFitness()
        {
            double fitness = 1;
            var first = genes[0];
            for (int i = 1; i < genomLength && genes[i] == first ; i++)
                    fitness++;
            if (!first)
                fitness = Form1.lolzcap;
            fitness = fitness / genomLength;
            return fitness; //da li ceo broj ili od 0 do 1, da li da vraca vrednost ili samo da updatuje
        }
        public override Individual New()
        {
            return new LolzBitArrayIndividual(genomLength);
        }
    }
}
