﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AI_Project_II
{
    public class OneMaxBitArrayIndividual : BitArrayIndividual
    {
        public OneMaxBitArrayIndividual()
        {

        }
        public OneMaxBitArrayIndividual(int size) : base(size)
        {
            fitness = 0;
        }
        public OneMaxBitArrayIndividual(int size, Random r) : base(size,r)
        {
            fitness = 0;
        }
        public override double calculateFitness()
        {
            //BitArray solution = new BitArray(genomLength);    //uciniti ga nekako globalnim
            //solution.SetAll(true);
            fitness = 0;
            for (int i = 0; i < genomLength; i++)
                if (Form1.onemaxsolution[i] == genes[i])
                    fitness++;
            fitness = fitness / genomLength;
            return fitness ; //da li ceo broj ili od 0 do 1, da li da vraca vrednost ili samo da updatuje
        }
        public override Individual New()
        {
            return new OneMaxBitArrayIndividual(genomLength);
        }
    }
}
