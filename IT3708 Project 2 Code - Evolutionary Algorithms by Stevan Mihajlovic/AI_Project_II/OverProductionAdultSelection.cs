﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class OverProductionAdultSelection : AdultSelection
    {
        
        public OverProductionAdultSelection()
        {

        }
        public override void selectAdults(Population population)
        {
            population.children = population.children.OrderByDescending(item => item.fitness).ToList();
            int i=0;
            int limit = population.populationSize / Form1.adultselection;
            population.parents.Clear();
            foreach (Individual individual in population.children)
            {
                population.parents.Add(individual);
                if ( i > limit)
                    break;
                i++;
            }                
        }
    }
}
