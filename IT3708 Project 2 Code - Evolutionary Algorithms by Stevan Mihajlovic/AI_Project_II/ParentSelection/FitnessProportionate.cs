﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class FitnessProportionate : ParentSelection
    {
        public FitnessProportionate()
        {

        }
        public override Individual selectParent(Population p, Random r)
        {
            double randomNumber = r.NextDouble() * p.totalFitness; //every individual has piece of cake, bigger fitess => bigger chance
            int index;
            for (index = 0; index < p.individuals.Count && randomNumber > 0; ++index)
            {
                randomNumber -= p.individuals.ElementAt(index).fitness;
            }
            return p.individuals.ElementAt(index - 1);
        }
    }
}
