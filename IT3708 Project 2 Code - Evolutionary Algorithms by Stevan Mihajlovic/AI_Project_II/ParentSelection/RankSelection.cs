﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class RankSelection : ParentSelection
    {
        public RankSelection()
        {

        }
        public override Individual selectParent(Population p, Random r)
        {
            double min = p.getMinFit().fitness;
            double max = p.getMaxFit().fitness;
            int n = p.populationSize;
            //sorted descending, 1st is best fit and his rank is n
            //foreach (Individual individual in population.individuals)
            //    individual.fitness = min + (max-min)*((n-1)/(population.populationSize-1));
            int i=0;
            foreach (Individual individual in p.individuals)
                individual.fitness = min + (max - min) * ((n - i++) / (double)(p.populationSize - 1));
            p.updatePopulationData();
            double randomNumber = r.NextDouble() * p.totalFitness;
            int index;
            for (index = 0; index < p.populationSize && randomNumber > 0; ++index)
            {
                randomNumber -= p.individuals.ElementAt(index).fitness;
            }
            p.updateFitnessForAll();
            return p.individuals.ElementAt(index - 1);
        }
    }
}
