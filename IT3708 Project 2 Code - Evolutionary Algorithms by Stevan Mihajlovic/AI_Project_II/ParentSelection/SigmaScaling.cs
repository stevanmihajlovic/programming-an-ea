﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    class SigmaScaling : ParentSelection
    {
        public SigmaScaling()
        {

        }
        public override Individual selectParent(Population p, Random r)
        {
            if (p.standardDeviation!=0)    //dividing by zero will collapse it
            foreach (Individual individual in p.individuals)
                individual.fitness = 1 + (individual.fitness - p.averageFitness) / (2 * p.standardDeviation);
            p.updatePopulationData();
            
            double randomNumber = r.NextDouble() * p.totalFitness;
            int index;
            for (index = 0; index < p.populationSize && randomNumber > 0; ++index)
            {
                randomNumber -= p.individuals.ElementAt(index).fitness;
            }
            p.updateFitnessForAll();
            return p.individuals.ElementAt(index - 1);
        }
    }
}
