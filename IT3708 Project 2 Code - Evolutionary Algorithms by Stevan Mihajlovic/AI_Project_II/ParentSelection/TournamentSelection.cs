﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class TournamentSelection : ParentSelection
    {
        public TournamentSelection()
        {

        }
        public override Individual selectParent(Population p, Random r)
        {
            Population newPopulation = new Population();
            for (int i = 0; i < Form1.tournamentSize; i++)
                newPopulation.individuals.Add(p.individuals[r.Next(p.populationSize)]); //ovde ako ne stavim prvo u zagradi uvek isto dobijem
            newPopulation.populationSize = Form1.tournamentSize;
            Individual newIndividual = newPopulation.getMaxFit();;
            return newIndividual;
        }
    }
}
