﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class Population
    {
        public List<Individual> parents;  //dodaj decu
        public List<Individual> children;
        public int populationSize;
        public int genomLength;
        public double totalFitness;
        public double averageFitness;
        public double standardDeviation;
        public AdultSelection adultSelection;   //napravi konstruktor
        public Random randomize;
        public ParentSelection parentSelection;

        public Population()
        {
            children = new List<Individual>();
            totalFitness = 0;
        }
        
        public Population(Population p)
        {
            populationSize = p.populationSize;
            genomLength = p.genomLength;
            randomize = p.randomize;            //vidi dal treba
            totalFitness = 0;
            children = new List<Individual>();
            parents = new List<Individual>();   //vidi dal treba
        }
        public Population(int size,int n, Random r, int problemtype, int adulttype,int parenttype)
        {
            randomize = r;
            populationSize = size;
            genomLength = n;
            children = new List<Individual>();
            parents = new List<Individual>();
            if (problemtype == 0)
                for (int i=0;i<populationSize;i++)
                    children.Add(new OneMaxBitArrayIndividual(genomLength,randomize));
            if (parenttype == 0)
                parentSelection = new TournamentSelectionParentSelection();
            if (adulttype == 0)
                adultSelection = new FullGenerationAdultSelection();
            totalFitness = 0;
        }
        public Individual getMaxFit()
        {
            Individual maxfit = children[0];
            for (int i = 1; i < populationSize; i++)
                if (maxfit.fitness < children[i].fitness)
                    maxfit = children[i];
            return maxfit;
        }
        public Individual getMinFit()
        {
            Individual minfit = children[0];
            for (int i = 1; i < populationSize; i++)
                if (minfit.fitness > children[i].fitness)
                    minfit = children[i];
            return minfit;
        }
        public void updateFitnessForAll()
        {
            foreach (Individual i in children)
                i.calculateFitness();
            updatePopulationData();
        }
        public void updatePopulationData()
        {
            totalFitness = 0;
            foreach (Individual i in children)
                totalFitness += i.fitness;
            averageFitness = totalFitness / populationSize;
            standardDeviation = 0;
            foreach (Individual i in children)
                standardDeviation += Math.Pow(i.fitness - averageFitness, 2);
            standardDeviation /= populationSize;
        }
        public void updatePopulationDataParents()
        {
            totalFitness = 0;
            foreach (Individual i in parents)
                totalFitness += i.fitness;
            averageFitness = totalFitness / populationSize;
            standardDeviation = 0;
            foreach (Individual i in parents)
                standardDeviation += Math.Pow(i.fitness - averageFitness, 2);
            standardDeviation /= populationSize;
        }
        
    }
}
