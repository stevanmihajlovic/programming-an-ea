﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class RankParentSelection : ParentSelection
    {
        public RankParentSelection()
        {

        }
        public override Individual selectParent(Population population, Random randomize)
        {
            double min = population.getMinFit().fitness;
            double max = population.getMaxFit().fitness;
            int n = population.populationSize;
            //sorted descending, 1st is best fit and his rank is n
            //foreach (Individual individual in population.individuals)
            //    individual.fitness = min + (max-min)*((n-1)/(population.populationSize-1));
            int i=0;
            foreach (Individual individual in population.individuals)
                individual.fitness = min + (max - min) * ((n - i++) / (double)(population.populationSize - 1));
            population.updatePopulationData();
            double randomNumber = randomize.NextDouble() * population.totalFitness;
            int index;
            for (index = 0; index < population.populationSize && randomNumber > 0; ++index)
            {
                randomNumber -= population.individuals.ElementAt(index).fitness;
            }
            population.updateFitnessForAll();
            return population.individuals.ElementAt(index - 1);
        }
    }
}
