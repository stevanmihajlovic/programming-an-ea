﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    class SigmaScalingParentSelection : ParentSelection
    {
        public SigmaScalingParentSelection()
        {

        }
        public override Individual selectParent(Population population, Random randomize)
        {
            if (population.standardDeviation!=0)    //dividing by zero will collapse it
            foreach (Individual individual in population.individuals)
                individual.fitness = 1 + (individual.fitness - population.averageFitness) / (2 * population.standardDeviation);
            population.updatePopulationData();
            
            double randomNumber = randomize.NextDouble() * population.totalFitness;
            int index;
            for (index = 0; index < population.populationSize && randomNumber > 0; ++index)
            {
                randomNumber -= population.individuals.ElementAt(index).fitness;
            }
            population.updateFitnessForAll();
            return population.individuals.ElementAt(index - 1);
        }
    }
}
