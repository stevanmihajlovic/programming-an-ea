﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    class SurpriseSequenceIntArrayIndividual : IntArrayIndividual
    {
        SurpriseSequenceIntArrayIndividual()
        {

        }
        SurpriseSequenceIntArrayIndividual(int size) : base (size)
        {

        }
        SurpriseSequenceIntArrayIndividual(int n, Random r, int min, int max) : base(n, r, min, max)
        {

        }
        SurpriseSequenceIntArrayIndividual(int n, Random r) : base (n, r)
        {

        }
        public override Individual New()
        {
            return new SurpriseSequenceIntArrayIndividual(genomLength);
        }
        public override double calculateFitness()
        {
            return 1;
        }
    }
}
