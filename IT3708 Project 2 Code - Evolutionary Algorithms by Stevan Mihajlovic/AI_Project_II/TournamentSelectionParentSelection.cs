﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Project_II
{
    public class TournamentSelectionParentSelection : ParentSelection
    {
        public TournamentSelectionParentSelection()
        {

        }
        public override Individual selectParent(Population population, Random randomize)
        {
            Population newPopulation = new Population();
            for (int i = 0; i < Form1.tournamentSize; i++)  //u pocetku je bilo od population.children[(int)(randomize.NextDouble() * population.children.Count)]
                newPopulation.children.Add(population.parents[(int)(randomize.NextDouble() * population.parents.Count)]);  //ovde ako ne stavim prvo u zagradi uvek isto dobijem
            Individual newIndividual = newPopulation.getMaxFit();   //max or fitness?
            return newIndividual;
        }
    }
}
